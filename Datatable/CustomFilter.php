<?php

namespace Fixitsoft\Datatable;

use Doctrine\ORM\QueryBuilder;

class CustomFilter
{
    private $columnName;

    /** @var \Closure $searchCondition */
    private $searchCondition;

    public function __construct(
        $searchCondition
    )
    {
        $this->searchCondition = $searchCondition;
    }

    public function filterCondition(QueryBuilder $qb, $searchValue){
        $searchCondition = $this->searchCondition;
        $qb = $searchCondition($qb,$searchValue);
        return $qb;
    }

    public function getColumnName(){
        return $this->columnName;
    }

    public function setColumnName($columnName){
        $this->columnName = $columnName;
    }
}