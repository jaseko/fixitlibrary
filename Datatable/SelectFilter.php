<?php

namespace Fixitsoft\Datatable;

use Fixitsoft\DateFunctions;
use Doctrine\ORM\QueryBuilder;

class SelectFilter
{

    private $columnName;
    private $options;


    /**
     * @param array $options
     * @param string|null $columnName
     */
    public function __construct(
        array $options,
        ?string $columnName = null
    )
    {
        $this->columnName = $columnName;
        $this->options = $options;
    }

    public function filterCondition(QueryBuilder $qb, $searchValue){

        $paramName = 'search_' . $this->columnName;
        $alias = $qb->getRootAliases()[0];
        $qb->andWhere($alias . '.' . $this->columnName . ' = :' . $paramName);
        $qb->setParameter($paramName, $searchValue);

        return $qb;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }



    public function getColumnName(){
        return $this->columnName;
    }

    public function isSelectFilter(){
        return true;
    }

    public function setColumnName($columnName){
        $this->columnName = $columnName;
    }
}