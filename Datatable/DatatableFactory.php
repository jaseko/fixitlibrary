<?php

namespace Fixitsoft\Datatable;

use Fixitsoft\DatatableColumn;
use Fixitsoft\DatatableDatatable;
use Fixitsoft\DatatableTextFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Twig\Environment;

class DatatableFactory
{

    /** @var Request $request */
    private $request;

    /** @var Datatable $dataTable */
    private $dataTable;

    /** @var TokenInterface $token */
    private $token;

    private $authorizationChecker;

    /** @var DatatableInterface $dt */
    private $dt;

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var RouterInterface $router */
    private $router;

    /** @var ContainerInterface $container */
    private $container;

    private $twig;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        EntityManagerInterface $em,
        RouterInterface $router,
        ContainerInterface $container,
        Environment $environment
    )
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->em = $em;
        $this->router = $router;
        $this->container = $container;
        $this->twig = $environment;
    }


    private function orderBy(){

        if (isset($this->dt->getOptions()['order'])) {
            $order = [

                    'column' => $this->dt->getOptions()['order']['column'],
                    'order' => $this->dt->getOptions()['order']['dir'],
                    'dir' => $this->dt->getOptions()['order']['dir']

            ];
            if(isset($order)) {
                $this->dataTable->setOrder($order);
            }
        }
    }
    /**
     * @param $class
     * @param Request $request
     * @return Fixitsoft\DatatableDatatable
     */
    public function create($class, Request $request, $user = null)
    {

        $this->request = $request;
        $this->dt = new $class($this->router, $this->em, $this->container, $this->twig);
        $this->dataTable = new Datatable(
            $this->dt->getName(),
            $this->dt->getRepository(),
            $request,
            $this->dt->getUrl(),
            $user,
            $this
        );

        $this->dataTable->setSearchTypes($this->dt->getCustomSearch());
        $columns = $this->dt->renderColumns();

        if (isset($this->dt->getOptions()['order'])) {
            foreach ($columns as $key => $column) {
                if (isset($this->dt->getOptions()['order'][$column->getColumn()])) {
                    $order = [
                        "column" => $key,
                        "dir" => $this->dt->getOptions()['order'][$column->getColumn()]
                    ];
                }
            }
            if(isset($order)) {
                $this->dataTable->setOrder($order);
            }
        }

        $this->dataTable->setColumns($columns);
        $this->orderBy();
        return $this->dataTable;
    }

    public function renderRow($entity)
    {
        $row = [];
        $columns = $this->dataTable->getColumns();
        foreach ($columns as $column) {
            $valFunction = $column->getValue();
            if ($valFunction != null && $valFunction instanceof \Closure) {
                $row[] = $valFunction($entity);
            } else {
                $row[] = '';
            }
        }
        return $row;
    }

    /**
     * @param Request $request
     */
    public function process()
    {

        $request = $this->request;

        /** @var Datatable $datatable */
        $datatable = $this->dataTable;

        $datatable->isAdmin(($this->authorizationChecker->isGranted('ROLE_ADMIN')));

        // custom search
        $searchTypeValue = (int)$request->request->get('searchType');

        /** @var QueryBuilder $qb */
        $qb = $datatable->render();

        $alias = $qb->getRootAliases()[0];
        if ($searchTypeValue) {
            /** @var SearchType $searchType */
            foreach ($this->dataTable->getSearchTypes() as $searchType) {
                if ($searchTypeValue == $searchType->getValue()) {
                    $closure = $searchType->getClosure();
                    $qb = $closure($qb, $alias);
                }
            }
        }

        $clients = $qb->getQuery()->getResult();

        $data = [];

        foreach ($clients as $client) {
            $data[] = $this->renderRow($client);
        }

        $json_data = array(
            "draw" => intval($request->request->get('draw')),
            "recordsTotal" => intval($datatable->getAllCount()),
            "recordsFiltered" => intval($datatable->getAllCount()),
            "data" => $data   // total data array
        );

        return $json_data;
    }


    /**
     * @return Datatable
     * @throws \Exception
     */
    public function getDataTable(): Datatable
    {
        if (empty($dataTable)) {
            throw new \Exception('Datatable is not created');
        }
        return $this->dataTable;
    }


}