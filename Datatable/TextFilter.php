<?php

namespace Fixitsoft\Datatable;

use Doctrine\ORM\QueryBuilder;

class TextFilter
{
    const OPERATOR_LIKE = 1;
    const OPERATOR_EQUAL = 2;

    private $columnName;
    private $operator;

    /**
     * @param string $operator
     * @param ?string $columnName
     */
    public function __construct(
        int $operator,
        ?string $columnName = null
    )
    {
        $this->operator = $operator;
        $this->columnName = $columnName;
    }

    public function filterCondition(QueryBuilder $qb, $searchValue){

        $paramName = 'search_'.$this->columnName;
        $alias = $qb->getRootAliases()[0];


        if($this->operator == self::OPERATOR_EQUAL) {
            $qb->andWhere($alias . '.' . $this->columnName . ' = :'.$paramName);
            $qb->setParameter($paramName, $searchValue);
        }elseif($this->operator == self::OPERATOR_LIKE){
            $qb->andWhere($alias . '.' . $this->columnName . ' LIKE :'.$paramName);
            $qb->setParameter($paramName, '%'.$searchValue.'%');
        }

        return $qb;
    }


    public function getColumnName(){
        return $this->columnName;
    }

    public function setColumnName($columnName){
        $this->columnName = $columnName;
    }

}