<?php

namespace Fixitsoft\Datatable;

use App\CoreLibrary\Datatable\FilterInterface;
use Symfony\Component\HttpFoundation\Request;

class Datatable
{
    const OPERATOR_LIKE = 1;
    const OPERATOR_EQUAL = 2;

    private $columnName;
    private $operator;
    private $em;
    private $repository;
    private $request;
    private $columns;
    private $url;
    private $filters;
    private $searchValues;
    private $name;
    private $order;
    private $token;
    private $datataTableFactory;
    private $searchTypes;
    private $additionalConditions;

    private $isAdmin = false;

    /**
     * @param object $repository
     * @param string $name
     * @param object $request
     * @param object $url
     * @param object $user
     * @param DatatableFactory $datataTableFactory
     */
    public function __construct(
        string $name,
        $repository,
        $request,
        $url,
        $user,
        DatatableFactory $datataTableFactory

    )
    {
        $this->name = $name;
        $this->url = $url;
        $this->request = $request;
        $this->repository = $repository;
        $this->offset = $request->request->get('start');
        $this->limit = $request->request->get('length');
        $this->search = $request->request->get('search')["value"];
        $this->order = isset($request->request->get('order')[0]) ? $request->request->get('order')[0] : null;
        $this->initColumns();
        $this->user = $user;
        $this->token = $this->name;
        $this->datataTableFactory = $datataTableFactory;
    }

    public function process(){
        return $this->datataTableFactory->process();
    }

    public function initColumns(){
        $request = $this->request;
        if($request->request->get('columns') != null) {
            foreach ($request->request->get('columns') as $key => $column) {
                if (!empty($column['name'])) {
                    $this->searchValues[$column['name']] = $column["search"]["value"];
                } else {
                    $this->searchValues[$key] = $column["search"]["value"];
                }
            }
        }
    }
    public function createQueryBuilder($alias){
        $qb = $this->repository->createQueryBuilder($alias);
        return $qb;
    }

    public function getFilters(){

        $filters = [];

        if($this->columns != null) {
            /** @var Column $column */
            foreach ($this->columns as $column) {
                if ($column->getFilter() != null) {
                    $filter = $column->getFilter();
                    if($filter->getColumnName() == null){
                        $filter->setColumnName($column->getColumn());
                    }

                    $filters[] = $column->getFilter();
                }
            }
        }

        return $filters;
    }

    public function editQuery(\Closure $closure){
        $this->additionalConditions = $closure;
    }

    public function render()
    {
        $qb =  $this->createQueryBuilder('b');
        $qb->where('b.id IS NOT NULL');

        if($this->isAdmin !== true){
            if($this->user != null) {
                $qb->andWhere('b.user = :user');
                $qb->setParameter('user', $this->user);
            }
        }

        if($this->additionalConditions != null){
            $closure = $this->additionalConditions;
            $qb = $closure($qb);
        }

        if($this->getFilters() != null){

            /** @var FilterInterface $filter */
            foreach($this->getFilters() as $filter){

                $searchValue = isset($this->searchValues[$filter->getColumnName()]) ? $this->searchValues[$filter->getColumnName()] : null;

                if(!empty($searchValue)) {
                    $qb = $filter->filterCondition($qb, $searchValue);
                }
            }
        }

        $qb
            ->setFirstResult($this->offset)
            ->setMaxResults($this->limit);


        if($this->order != null && !empty($this->columns[(int)$this->order["column"]])){
            $column = $this->columns[(int)$this->order["column"]];
            $qb->orderBy('b.'.$column->getColumn(), $this->order["dir"]);
        }else{
            $qb->orderBy('b.id', 'DESC');
        }


        return $qb;
    }

    public function isRequested(Request $request){
        $token = $request->request->get('token');
        if($this->token == $token){
            return true;
        }
    }


    public function getAllCount()
    {
        $qb =  $this->createQueryBuilder('b');
        $qb->select('count(b.id)');

        if($this->isAdmin !== true){
            if($this->user != null) {
                $qb->andWhere('b.user = :user');
                $qb->setParameter('user', $this->user);
            }
        }
        if($this->additionalConditions != null){
            $closure = $this->additionalConditions;
            $qb = $closure($qb);
        }
        if($this->getFilters() != null){

            /** @var FilterInterface $filter */
            foreach($this->getFilters() as $filter){

                $searchValue = isset($this->searchValues[$filter->getColumnName()]) ? $this->searchValues[$filter->getColumnName()] : null;

                if(!empty($searchValue)) {
                    $qb = $filter->filterCondition($qb, $searchValue);
                }
            }
        }

        $searchTypeValue = (int)$this->request->request->get('searchType');

        if($searchTypeValue) {
            /** @var SearchType $searchType */
            foreach($this->getSearchTypes() as $searchType){
                if($searchTypeValue == $searchType->getValue()) {
                    $alias = $qb->getRootAliases()[0];
                    $closure = $searchType->getClosure();
                    $qb = $closure($qb, $alias);
                }
            }
        }

        if($this->order != null && !empty($this->columns[$this->order["column"]])){
            $column = $this->columns[$this->order["column"]];
            $qb->orderBy('b.'.$column->getColumn(), $this->order["dir"]);
        }else{
            $qb->orderBy('b.id', 'DESC');
        }

        return  $qb->getQuery()->getSingleScalarResult();
    }

    public static function setCustomRowClass($data, $class){
        if(isset($data[0])){
            $td = '<span data-class="'.$class.'" class="custom-row"></span>'.$data[0];
            $data[0] = $td;
        }
        return $data;
    }

    public static function likeSearch($value){
        return '%'.$value.'%';
    }

    public static function alias($alias, $property, $condition = null){
        if($condition == null){
            $s = sprintf('%s.%s', $alias, $property);
            return $s;
        }
        $s = sprintf('%s.%s %s', $alias, $property, $condition);
        return $s;
    }
    public function setColumns($columns){
        $cols = [];
        /** @var Column $column */
        foreach($columns as $column){
            if($column->getCondition() !== null){
                if($column->getCondition() == true){
                    $cols[] = $column;
                }
            }else{
                $cols[] = $column;
            }
        }

        $this->columns = $cols;
    }

    public function getColumns(){
        return $this->columns;
    }

    public function setFilters($filters){
        $this->filters = $filters;
    }


    public function createColumn($column, $filter){

    }

    public function isAdmin($isAdmin){
        $this->isAdmin  = $isAdmin;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return array |null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param array | null $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getSearchTypes()
    {
        return $this->searchTypes;
    }

    /**
     * @param mixed $searchTypes
     */
    public function setSearchTypes($searchTypes): void
    {
        if($searchTypes != null) {
            $value = 1;
            foreach ($searchTypes as $searchType) {
                $searchType->setValue($value);
                $value++;
                $this->searchTypes[] = $searchType;
            }
        }
    }








}