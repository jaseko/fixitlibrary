<?php

namespace Fixitsoft\Datatable;

use Doctrine\ORM\QueryBuilder;

class NullFilter
{
    const NULL = 1;
    const NOT_NULL = 2;

    private $columnName;
    private $operator;

    /**
     * @param string $operator
     * @param ?string $columnName
     */
    public function __construct(
        int $operator,
        ?string $columnName = null
    )
    {
        $this->operator = $operator;
        $this->columnName = $columnName;
    }

    public function filterCondition(QueryBuilder $qb, $searchValue = null){

        $alias = $qb->getRootAliases()[0];

        if($this->operator == self::NULL)
            $qb->andWhere($alias . '.' . $this->columnName . ' IS NULL');
        elseif($this->operator == self::NOT_NULL)
            $qb->andWhere($alias . '.' . $this->columnName . ' IS NOT NULL');
        else
            throw new \Exception(409, "Chybějící parameter v konstruktoru nebo chybová hodnota");

        return $qb;
    }


    public function getColumnName(){
        return $this->columnName;
    }

    public function setColumnName($columnName){
        $this->columnName = $columnName;
    }
}