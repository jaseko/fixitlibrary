<?php

namespace Fixitsoft\Datatable;

use App\CoreLibrary\Functions\DateFunctions;
use Doctrine\ORM\QueryBuilder;

class DateFilter
{

    private $columnName;
    private $format;


    /**
     * @param string $format
     * @param ?string $columnName
     */
    public function __construct(
        string $format,
        ?string $columnName = null
    )
    {
        $this->format = $format;
        $this->columnName = $columnName;
    }

    public function filterCondition(QueryBuilder $qb, $searchValue){

        $dateTimeSearchValue = date_create_from_format(DateFunctions::phpDateFormat($this->format), $searchValue);
        $dateTimeSearchValue->setTime(0,0,0,0);

        if($dateTimeSearchValue) {
            $paramName = 'search_' . $this->columnName;
            $alias = $qb->getRootAliases()[0];
            $qb->andWhere($alias . '.' . $this->columnName . ' = :' . $paramName);
            $qb->setParameter($paramName, $dateTimeSearchValue);
        }
        return $qb;
    }


    public function getColumnName(){
        return $this->columnName;
    }

    public function isDateFilter(){
        return true;
    }

    public function setColumnName($columnName){
        $this->columnName = $columnName;
    }
}