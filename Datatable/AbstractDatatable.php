<?php declare(strict_types=1);

namespace Fixitsoft\Datatable;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractDatatable{

    private $name;

    private $repository;

    private $url;

    private $searchTypes;

    private $options;

    public function setInit(array $options, EntityManagerInterface $em){

        $this->setName($options['name']);
        $this->setUrl($options['url']);
        $this->setRepository($em->getRepository($options['entity']));
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param mixed $repository
     */
    public function setRepository($repository): void
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getSearchTypes()
    {
        return $this->searchTypes;
    }

    /**
     * @param mixed $searchTypes
     */
    public function setSearchTypes($searchTypes): void
    {
        $this->searchTypes = $searchTypes;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }





}