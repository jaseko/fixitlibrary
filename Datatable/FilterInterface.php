<?php

namespace Fixitsoft\Datatable;

use Doctrine\ORM\QueryBuilder;

interface FilterInterface{

    public function filterCondition(QueryBuilder $qb, $searchValue = null);

    public function getColumnName();
    public function setColumnName();

}