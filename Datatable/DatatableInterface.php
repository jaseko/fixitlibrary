<?php

namespace Fixitsoft\Datatable;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

interface DatatableInterface{


    public function setInit(array $options, EntityManagerInterface $em);
    public function renderColumns();
    public function getCustomSearch();

    public function getName();
    public function getUrl();
    public function getRepository();


}