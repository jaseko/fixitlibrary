<?php declare(strict_types=1);

namespace Fixitsoft\Functions;

class DateFunctions{

    public static function phpDateFormat($format){

        if($format == 'dd.mm.yyyy'){
            return 'd.m.Y';
        }

        return '';
    }

    public static function dateTimeOutput(?\DateTime $dateTime, $format){
        if($dateTime != null){
            return $dateTime->format($format);
        }

        return '-';
    }
}